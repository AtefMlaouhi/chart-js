import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'chartjs', loadChildren: () => import('./features/chartjs/chartjs.module').then(m => m.ChartjsModule) },
  { path: 'ng2chart', loadChildren: () => import('./features/ng2chart/ng2chart.module').then(m => m.Ng2chartModule) },
  { path: 'swimlane', loadChildren: () => import('./features/swimlane/swimlane.module').then(m => m.SwimlaneModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
