export const single = [
  {
    name: 'Sediane',
    borderColor: '#5AA454',
    series: [
      {
        name: '2000',
        value: '13030'
      },
      {
        name: '2001',
        value: '45000'
      },
      {
        name: '2002',
        value: '9323'
      },
      {
        name: '2003',
        value: '1500'
      },
      {
        name: '2004',
        value: '4020'
      },
      {
        name: '2005',
        value: '3500'
      },
      {
        name: '2006',
        value: '30340'
      },
      {
        name: '2007',
        value: '20000'
      },
      {
        name: '2008',
        value: '25000'
      },
      {
        name: '2009',
        value: '30000'
      },
      {
        name: '2010',
        value: '27990'
      },
      {
        name: '2011',
        value: '23780'
      },
      {
        name: '2012',
        value: '30330'
      },
      {
        name: '2013',
        value: '18456'
      },
      {
        name: '2014',
        value: '4500'
      },
      {
        name: '2015',
        value: '4500'
      },
      {
        name: '2016',
        value: '4200'
      },
      {
        name: '2017',
        value: '14500'
      },
      {
        name: '2018',
        value: '19000'
      },
      {
        name: '2019',
        value: '23945'
      }
    ],
  },
  {
    name: 'Sirène Nord',
    borderColor: '#A10A28',
    series: [
      {
        name: '2012',
        value: '0'
      },
      {
        name: '2013',
        value: '28456'
      },
      {
        name: '2014',
        value: '14500'
      },
      {
        name: '2015',
        value: '8500'
      },
      {
        name: '2016',
        value: '4000'
      },
      {
        name: '2017',
        value: '4500'
      },
      {
        name: '2018',
        value: '10000'
      },
      {
        name: '2019',
        value: '15000'
      }
    ],
  },
  {
    name: 'Serene Atlantique',
    borderColor: '#1976d2',
    series: [
      {
        name: '2010',
        value: '0'
      },
      {
        name: '2011',
        value: '2887'
      },
      {
        name: '2012',
        value: '430'
      },
      {
        name: '2013',
        value: '8456'
      },
      {
        name: '2014',
        value: '255'
      },
      {
        name: '2015',
        value: '2500'
      },
      {
        name: '2016',
        value: '5000'
      },
      {
        name: '2017',
        value: '8000'
      },
      {
        name: '2018',
        value: '15000'
      },
      {
        name: '2019',
        value: '55000'
      }
    ],
  }

];
