import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { Chart, ChartDataSets } from 'chart.js';
import { single } from 'src/app/data';

@Component({
  selector: 'app-chartjs',
  templateUrl: './chartjs.component.html',
  styleUrls: ['./chartjs.component.scss']
})
export class ChartjsComponent implements AfterViewInit {
  single: ChartDataSets[];
  canvas: any;
  ctx: any;
  @ViewChild('mychart') mychart;

  constructor() {
    Object.assign(this, { single });
  }


  ngAfterViewInit(): void {
    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    const myChart = new Chart(this.ctx, {
      type: 'line',

      data: {
        datasets: this.single.reduce((a, currentValue) =>
          [...a, {
            label: currentValue['name'],
            backgroundColor: 'transparent',
            borderColor: currentValue['borderColor'],
            fill: true,
            data: (currentValue['series'] as []).map(serieItem => ({ x: serieItem['name'], y: serieItem['value'] }))
          }], [])
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Evolution Stock'
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              labelString: 'année',
              display: true,
            }
          }],
          yAxes: [{
            type: 'linear',
            scaleLabel: {
              labelString: 'KWH',
              display: true
            }
          }]
        }
      }
    });
  }

}
