import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartjsRoutingModule } from './chartjs-routing.module';
import { ChartjsComponent } from './chartjs.component';


@NgModule({
  declarations: [ChartjsComponent],
  imports: [
    CommonModule,
    ChartjsRoutingModule
  ]
})
export class ChartjsModule { }
