import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { Ng2chartComponent } from './ng2chart.component';

const routes: Routes = [{ path: '', component: Ng2chartComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, ChartsModule]
})
export class Ng2chartRoutingModule { }
