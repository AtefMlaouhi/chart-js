import { Component, OnInit } from '@angular/core';
import { single } from 'src/app/data';

@Component({
  selector: 'app-ng2chart',
  templateUrl: './ng2chart.component.html',
  styleUrls: ['./ng2chart.component.scss']
})
export class Ng2chartComponent {
  single: any[];

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['sagar', 'laxman', 'nimesh', 'vishal', 'nilam'];
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData = [];

  public barChartOptions1 = {
    options: {
      title: {
        display: true,
        text: 'Example Chart'
      },
      scales: {
        xAxes: [{
          position: 'bottom',
          gridLines: {
            zeroLineColor: 'rgba(0,255,0,1)'
          },
          scaleLabel: {
            display: true,
            labelString: 'x axis'
          },
          stacked: true
        }],
        yAxes: [{
          position: 'left',
          gridLines: {
            zeroLineColor: 'rgba(0,255,0,1)'
          },
          scaleLabel: {
            display: true,
            labelString: 'y axis'
          }
        }]
      }
    }
  };


  constructor() {
    Object.assign(this, { single });
    this.barChartData = single.reduce((a, currentValue) =>
      [...a, {
        label: currentValue['name'],
        backgroundColor: 'transparent',
        borderColor: currentValue['borderColor'],
        fill: true,
        data: (currentValue['series'] as []).map(serieItem => ({ x: serieItem['name'], y: serieItem['value'] }))
      }], []);

    this.barChartLabels = single.reduce((a, currentValue) =>
      [...a,
      (currentValue['series'] as []).map(serieItem => (serieItem['name']))
      ], []).reduce((a, b) =>
        a.length > b.length ? a : b
      );
  }
}
