import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ng2chartRoutingModule } from './ng2chart-routing.module';
import { Ng2chartComponent } from './ng2chart.component';


@NgModule({
  declarations: [Ng2chartComponent],
  imports: [
    CommonModule,
    Ng2chartRoutingModule
  ]
})
export class Ng2chartModule { }
