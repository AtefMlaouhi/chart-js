import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SwimlaneComponent } from './swimlane.component';

const routes: Routes = [{ path: '', component: SwimlaneComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwimlaneRoutingModule { }
