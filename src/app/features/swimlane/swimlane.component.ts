import { Component } from '@angular/core';
import { single } from 'src/app/data';
import * as Chart from 'chart.js';


@Component({
  selector: 'app-swimlane',
  templateUrl: './swimlane.component.html',
  styleUrls: ['./swimlane.component.scss']
})
export class SwimlaneComponent {
  single: any[];
  public view: any[] = [1000, 400];
  public showXAxis = true;
  public showYAxis = true;
  public gradient = false;
  public showLegend = true;
  public showXAxisLabel = true;
  public xAxisLabel: 'Years';
  public showYAxisLabel = true;
  public yAxisLabel: 'Stock';
  public graphDataChart: any[];
  public colorScheme = {
    domain: ['#5AA454', '#A10A28', '#1976d2']
  };

  ///////////////
  chart: Chart;

  constructor() {
    Object.assign(this, { single });
  }
}
