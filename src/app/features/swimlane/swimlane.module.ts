import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwimlaneRoutingModule } from './swimlane-routing.module';
import { SwimlaneComponent } from './swimlane.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';


@NgModule({
  declarations: [SwimlaneComponent],
  imports: [
    CommonModule,
    NgxChartsModule,
    SwimlaneRoutingModule
  ]
})
export class SwimlaneModule { }
